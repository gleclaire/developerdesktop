#!/usr/bin/env bash

if [ $(id -u) -ne 0 ]; then
  echo "This script must be run as root";
  exit 1;
fi

sudo apt-get update

# Install base utils for docker
sudo apt remove docker docker-engine docker.io
sudo apt-get -y install apt-transport-https ca-certificates curl software-properties-common

# Adding Docker’s GPG Key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88
# Now install the Docker repository using the command:
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu  $(lsb_release -cs)  stable"


# Installing the Latest Docker
sudo apt-get -y install docker-ce

sudo systemctl start docker

# Enable Docker to run at startup with:
sudo systemctl enable docker

# check the status of the service:
sudo systemctl status docker

sudo chmod 666 /var/run/docker.sock

sudo groupadd docker
sudo usermod -aG docker $USER

sudo apt-get clean
