#!/usr/bin/env bash

if [ $(id -u) -ne 0 ]; then
  echo "This script must be run as root";
  exit 1;
fi

sudo tee "/etc/systemd/system/docker.service.d/http-proxy.conf" > "/dev/null" <<EOF

[Service]
Environment="HTTP_PROXY=#{ENV['HTTP_PROXY']}" "NO_PROXY=#{ENV['NO_PROXY']}"

EOF

tee "/etc/systemd/system/docker.service.d/https-proxy.conf" > "/dev/null" <<EOF

[Service]
Environment="HTTPS_PROXY=#{ENV['HTTPS_PROXY']}" "NO_PROXY=#{ENV['NO_PROXY']}"

EOF


sudo tee "/etc/systemd/system/docker.service.d/docker.conf" > "/dev/null" <<EOF

[Service]
ExecStart=
ExecStart=/usr/bin/dockerd


EOF

sudo tee "/etc/docker/daemon.json" > "/dev/null" <<EOF

{
  "dns": ["10.1.0.13"],
  "metrics-addr" : "127.0.0.1:9323",
  "experimental" : true
}

EOF

sudo service docker restart

