#!/usr/bin/env bash

if [ $(id -u) -ne 0 ]; then
  echo "This script must be run as root";
  exit 1;
fi

sudo apt-get update

# Install base utils to allow Ubuntu updates
sudo apt-get -y --force-yes install gcc make perl curl

# Install base utils to allow development
sudo apt-get -y --force-yes install build-essential file git

sudo apt-get clean


