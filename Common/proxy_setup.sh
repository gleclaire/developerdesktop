#!/usr/bin/env bash


PROXY_HOST=proxy.mycompany:80

#export http_proxy=http://my-company-proxy.com:80
#export https_proxy=http://my-company-proxy.com:80


# Git setup
git config --global http.proxy http://$PROXY_HOST

#Server: myproxyserver
#Port: 8080
#Username: mydomain\myusername
#Password: mypassword
#git config --global http.proxy http://mydomain\\myusername:mypassword@myproxyserver:8080

# NPM setup

#npm config set cafile C:\path\to\certs.pem
#npm config set strict-ssl=false
#export NODE_TLS_REJECT_UNAUTHORIZED=0
#npm config set proxy http://<username>:<password>@<proxy-server-url>:<port>
#npm config set https-proxy http://<username>:<password>@<proxy-server-url>:<port>