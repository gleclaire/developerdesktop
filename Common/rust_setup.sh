#!/usr/bin/env bash

# Rust Initializer
brew install rustup-init

rustup-init -y

rustup -V
rustup show
rustup update

rustc -V
cargo -V

cargo install wrangler
