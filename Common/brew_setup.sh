#!/usr/bin/env bash

brew tap versent/homebrew-taps
brew tap aws/tap
brew tap weaveworks/tap
brew install bash-completion

# Dev Tools ( you can run from Docker instead)
brew install go node python

# Golang Dev Tools ( you can run from Docker instead)
brew install go gopls

# Utilities
brew install unzip xz zip
brew install jq jo yq
brew install fselect
brew install node graphviz
brew install golang-migrate
brew install mkcert
brew install dasel
brew install benthos


# Data Tools
brew install cuelang/tap/cue


# Install Docker in  MacOS or Ubuntu with WSL1
# Do not install with WSL2
# Do not install if using Docker Desktop in MacOS or Windows
# brew install docker docker-compose

# Hyperkit if not using Docker Desktop otherwise it is included
# brew install hyperkit

# Docker Tools
brew install dive ctop
brew install hadolint
brew install reg
brew install docker-ls
# https://github.com/patrickhoefler/dockerfilegraph
brew install patrickhoefler/tap/dockerfilegraph
# https://github.com/google/ko
brew install ko
# https://github.com/google/go-containerregistry/blob/main/cmd/crane/README.md
$ brew install crane


# https://github.com/lima-vm/lima
brew install lima


#Amazon Tools
brew install amazon-ecs-cli awscli awslogs aws-sam-cli aws-shell copilot-cli saml2aws
brew install cli53
brew install ec2-instance-selector
brew tap komiserio/komiser
brew install komiser
# Cloudformation tool
brew install rain


# Encore Go Framework
brew install encoredev/tap/encore

#Terraform
brew install terraform terraform-docs terraformer terraforming

#CDK
brew install aws-cdk cdk8s cdktf

# miniKube
brew install minikube

# Kubernetes Tools
brew install helm k9s kubernetes-cli kubernetes-helm kubens kubectx
brew install weaveworks/tap/eksctl
brew install kdash-rs/kdash/kdash
brew install txn2/tap/kubefwd
# Tilt
# https://tilt.dev/
brew install tilt-dev/tap/ctlptl
brew install tilt-dev/tap/tilt


# testing tools
brew install hey

# Snyk
brew tap snyk/tap
brew install snyk

# Minio Client
brew install minio/stable/mc

# Anchor Tools
brew tap anchore/syft
brew install syft
brew tap anchore/grype
brew install grype

# Database Tools
brew install sqlc

# Github Tools
brew install git
brew tap microsoft/git
brew install --cask git-credential-manager-core
brew install gh

# certinfo
# https://github.com/pete911/certinfo
brew tap pete911/tap
brew install certinfo

brew cleanup

brew list


