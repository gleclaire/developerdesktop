#!/usr/bin/env bash

# CDK version updater
# https://github.com/cdk-dev/bump-cdk
npm i -g bump-cdk
npm i rocketcdk
npm install -g cdk-import

npm i -g cdk-dia


#Depcheck is a tool for analyzing the dependencies in a project
# https://github.com/depcheck/depcheck
#npm install -g depcheck
npm install -g depcheck typescript