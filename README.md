# Developer Desktop Setup #

This will outline how to setup MacOS, Windows and/or Linux for Development.

### Docker Development ###

This is will be done on Windows 10 with [Docker Desktop](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact