#!/usr/bin/env bash

#Link ssh
ln -s /mnt/c/Users/leclgard/.ssh ~/.ssh
sudo chmod 600 ~/.ssh/id_rsa

#Link aws
ln -s /mnt/c/Users/leclgard/.aws ~/.aws

#Link azure
ln -s /mnt/c/Users/leclgard/.azure ~/.azure