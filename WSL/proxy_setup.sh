#!/usr/bin/env bash


docker run --name squid -d -p 3128:3128 datadog/squid

export http_proxy=http://127.0.0.1:3128

export https_proxy=http://127.0.0.1:3128
