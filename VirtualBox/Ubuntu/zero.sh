#!/usr/bin/env bash

if [ $(id -u) -ne 0 ]; then
  echo "This script must be run as root";
  exit 1;
fi

echo "Apt cleanup....."
sudo apt-get clean

echo "Zero Empty sectors....."
dd if=/dev/zero of=/var/tmp/bigemptyfile bs=4096k

echo "Removing Temp files"
rm /var/tmp/bigemptyfile

echo "All Done"
