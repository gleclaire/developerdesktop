Write-Host 'HOMEDRIVE: ' $env:HOMEDRIVE
Write-Host 'HOMEPATH: ' $env:HOMEPATH

$HOME_DIR=$env:HOMEDRIVE + $env:HOMEPATH

Write-Host 'HOME_DIR: ' $HOME_DIR
Write-Host  ""

# args[0] is the VBox Host name.
if  ($null -eq $args[0]) {

  Write-Host 'Enter HOST_NAME Parameter'  
  exit 1;
} else {
  $HOST_NAME=$args[0]
}


# args[1] is the Port name.
if  ($null -eq $args[1]) {
  Write-Host 'Enter PORT_NUMBER Parameter'  
  exit 1;
} else {
  $PORT_NUMBER=$args[1]
}

$PORT_ALIAS="PORT_" + $PORT_NUMBER 

Write-Host "HOST_NAME: $HOST_NAME"
Write-Host "PORT_NUMBER: $PORT_NUMBER"
Write-Host "PORT_ALIAS: $PORT_ALIAS"
Write-Host ""
Write-Host ""

Write-Host "Removing Port: $PORT_NUMBER mapping to Host: $VDI_NAME as Alias: $PORT_ALIAS ...."
Invoke-Expression 'VBoxManage modifyvm "$HOST_NAME" --natpf1 delete "$PORT_ALIAS"'

