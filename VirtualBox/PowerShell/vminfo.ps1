
# args[0] is the VBox Host name.

if  ($null -eq $args[0]) {

  Write-Host 'Enter HOST_NAME Parameter'  
  exit 1;
} else {
  $HOST_NAME=$args[0]
}


Write-Host  'HOST_NAME: $HOST_NAME'

# set HOST_NAME=Ubuntu-18.04

$command = 'VBoxManage showvminfo $HOST_NAME'

Invoke-Expression $command

