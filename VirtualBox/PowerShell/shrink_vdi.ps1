
Write-Host 'HOMEDRIVE: ' $env:HOMEDRIVE
Write-Host 'HOMEPATH: ' $env:HOMEPATH

$HOME_DIR=$env:HOMEDRIVE + $env:HOMEPATH

Write-Host 'HOME_DIR: ' $HOME_DIR
Write-Host  ""

$IMG_BASE_LOCATION=$HOME_DIR + '\VirtualBox VMs'

# args[0] is the VBox Host name.
if  ($null -eq $args[0]) {

  Write-Host 'Enter HOST_NAME Parameter'  
  exit 1;
} else {
  $HOST_NAME=$args[0]
}

Write-Host  "HOST_NAME: $HOST_NAME"

$IMG_LOCATION=$IMG_BASE_LOCATION + "\$HOST_NAME"
$VDI_NAME=$IMG_LOCATION + "\$HOST_NAME.vdi"

Write-Host "IMG_BASE_LOCATION: $IMG_BASE_LOCATION"
Write-Host "IMG_LOCATION: $IMG_LOCATION"
Write-Host "HOST_NAME: $HOST_NAME"
Write-Host "VDI_NAME: $VDI_NAME"
Write-Host ""
Write-Host ""

Write-Host "Compacting $VDI_NAME ...."
Invoke-Expression 'VBoxManage modifymedium --compact "$VDI_NAME"'


