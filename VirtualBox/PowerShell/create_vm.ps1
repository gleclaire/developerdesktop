
Write-Host 'HOMEDRIVE: ' $env:HOMEDRIVE
Write-Host 'HOMEPATH: ' $env:HOMEPATH

$HOME_DIR=$env:HOMEDRIVE + $env:HOMEPATH

Write-Host 'HOME_DIR: ' $HOME_DIR
Write-Host  ""

$OSTYPE='Linux_64'
$HOST_MEMORY=8192
$HD_SIZE=200000
$VIDEO_MEMORY=16
$IMG_BASE_LOCATION=$HOME_DIR + '\VirtualBox VMs'
$SHARE_FOLDER=$HOME_DIR + '\Downloads'
$BOOT_CD=$HOME_DIR + '\Downloads\ubuntu-20.04.1-desktop-amd64.iso'

Write-Host  "HOST_MEMORY: $HOST_MEMORY"
Write-Host  "HD_SIZE: $HD_SIZE"
Write-Host  "VIDEO_MEMORY: $VIDEO_MEMORY"
Write-Host  "SHARE_FOLDER: $SHARE_FOLDER"
Write-Host  "BOOT_CD: $BOOT_CD"
Write-Host  ""

# args[0] is the VBox Host name.
if  ($null -eq $args[0]) {

  Write-Host 'Enter HOST_NAME Parameter'  
  exit 1;
} else {
  $HOST_NAME=$args[0]
}

Write-Host  "HOST_NAME: $HOST_NAME"

# args[1] is the VDI Image.
if  ($null -ne $args[1]) {
  $VDI_MASTER=$args[1]
}


Write-Host  "HOST_NAME: $HOST_NAME"

$IMG_LOCATION=$IMG_BASE_LOCATION + "\$HOST_NAME"

# HOST_NAME=Ubuntu-18.04
$VDI_NAME=$IMG_LOCATION + "\$HOST_NAME.vdi"

Write-Host "OSTYPE: $OSTYPE"

Write-Host "IMG_BASE_LOCATION: $IMG_BASE_LOCATION"
Write-Host "IMG_LOCATION: $IMG_LOCATION"
Write-Host "HOST_NAME: $HOST_NAME"
Write-Host "VDI_NAME: $VDI_NAME"
Write-Host  "VDI_MASTER: $VDI_MASTER"
Write-Host  ""
Write-Host  ""


Write-Host "Creating Host: $HOST_NAME ...."
Invoke-Expression 'VBoxManage createvm --ostype "$OSTYPE" --name "$HOST_NAME" --register'

Invoke-Expression 'VBoxManage modifyvm $HOST_NAME --memory $HOST_MEMORY --vram $VIDEO_MEMORY --graphicscontroller vmsvga --rtcuseutc on'

Invoke-Expression 'VBoxManage modifyvm $HOST_NAME --acpi on --ioapic on --pae off --nestedpaging on --vtxvpid on --paravirtprovider default'

Invoke-Expression 'VBoxManage modifyvm $HOST_NAME --clipboard-mode bidirectional --draganddrop bidirectional --audio none'


Invoke-Expression 'VBoxManage storagectl $HOST_NAME --name "SATA Controller" --add sata --controller IntelAhci --portcount 1'


if  ($null -eq $VDI_MASTER) {
  Write-Host "Creating Disk: $VDI_NAME ...."
  Invoke-Expression 'VBoxManage createmedium disk --filename "$VDI_NAME" --size $HD_SIZE --format VDI'
} else {
  Write-Host "Cloning Disk: $VDI_MASTER to $VDI_NAME ...."
#  Invoke-Expression 'VBoxManage closemedium disk "$VDI_NAME"'
  Invoke-Expression 'VBoxManage clonemedium disk "$VDI_MASTER" "$VDI_NAME"'
} 

Write-Host "Attacking Disk: $VDI_NAME to Storage Controller...."
Invoke-Expression 'VBoxManage storageattach $HOST_NAME --storagectl "SATA Controller" --port 0 --device 0 --type hdd --medium $VDI_NAME'

Invoke-Expression 'VBoxManage storagectl $HOST_NAME --name "IDE Controller" --add ide --controller PIIX4'

Invoke-Expression 'VBoxManage modifyvm $HOST_NAME --boot1 dvd --boot2 disk --boot3 none --boot4 none'

Invoke-Expression 'VBoxManage sharedfolder add $HOST_NAME --name vm_utils --hostpath "$SHARE_FOLDER" --automount'

# Invoke-Expression 'VBoxManage storageattach $HOST_NAME --storagectl "IDE Controller" --port 1 --device 0 --type dvddrive --medium $BOOT_CD

# Invoke-Expression 'VBoxManage showvminfo "$HOST_NAME"'
