#!/usr/bin/env bash

OSTYPE="Linux_64"
HOST_MEMORY=8192
HD_SIZE=200000
VIDEO_MEMORY=16

#$1 is the VBox Host name.
if [ -n "$1" ]
then
  HOST_NAME="$1"
else
  echo "Enter HOST_NAME Parameter"
  exit 1;
fi

echo  "$HOST_NAME"

IMG_LOCATION="$HOME/Documents/VirtualMachines/VirtualBox/$HOST_NAME"

# HOST_NAME=Ubuntu-18.04
VDI_NAME="$IMG_LOCATION/$HOST_NAME.vdi"

echo "OSTYPE: $OSTYPE"


echo "IMG_LOCATION: $IMG_LOCATION"
echo "HOST_NAME: $HOST_NAME"
echo "VDI_NAME: $VDI_NAME"


echo "Creating $VDI_NAME ...."
VBoxManage createvm --ostype "$OSTYPE" --name "$HOST_NAME" --register

VBoxManage modifyvm $HOST_NAME --memory $HOST_MEMORY --vram $VIDEO_MEMORY --graphicscontroller vmsvga --rtcuseutc on

VBoxManage modifyvm $HOST_NAME --acpi on --ioapic on --pae off --nestedpaging on --vtxvpid on --paravirtprovider default

VBoxManage modifyvm $HOST_NAME --clipboard-mode bidirectional --draganddrop bidirectional --audio none

VBoxManage createhd --filename "$VDI_NAME" --size $HD_SIZE --format VDI

VBoxManage storagectl $HOST_NAME --name "SATA Controller" --add sata --controller IntelAhci --portcount 1
VBoxManage storageattach $HOST_NAME --storagectl "SATA Controller" --port 0 --device 0 --type hdd --medium $VDI_NAME
VBoxManage storagectl $HOST_NAME --name "IDE Controller" --add ide --controller PIIX4

VBoxManage modifyvm $HOST_NAME --boot1 dvd --boot2 disk --boot3 none --boot4 none

VBoxManage sharedfolder add $HOST_NAME --name vm_utils --hostpath "$HOME/Downloads" --automount

VBoxManage storageattach $HOST_NAME --storagectl "IDE Controller" --port 1 --device 0 --type dvddrive --medium $HOME/Downloads/ubuntu-20.04.1-desktop-amd64.iso


# VBoxManage showvminfo "$HOST_NAME"


