#!/usr/bin/env bash

#$1 is the VBox Host name.

if [ -n "$1" ]
then
  HOST_NAME="$1"
else
  echo "Enter HOST_NAME Parameter"  
  exit 1;
fi


echo  "$HOST_NAME"

date

VBoxManage modifyvm "$HOST_NAME" --natpf1 delete "store-server"
VBoxManage modifyvm "$HOST_NAME" --natpf1 delete "shelf-server"
VBoxManage modifyvm "$HOST_NAME" --natpf1 delete "activemq-server"
VBoxManage modifyvm "$HOST_NAME" --natpf1 delete "store-ui"
VBoxManage modifyvm "$HOST_NAME" --natpf1 delete "postgres"
VBoxManage modifyvm "$HOST_NAME" --natpf1 delete "enterprise"
VBoxManage modifyvm "$HOST_NAME" --natpf1 delete "ent-micro-service"

date


