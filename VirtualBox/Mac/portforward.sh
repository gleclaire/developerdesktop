#!/usr/bin/env bash

#$1 is the VBox Host name.

if [ -n "$1" ]
then
  HOST_NAME="$1"
else
  echo "Enter HOST_NAME Parameter"
  exit 1;
fi


echo  "$HOST_NAME"

date

VBoxManage modifyvm "$HOST_NAME" --natpf1 "store-server,tcp,,40080,,40080"
VBoxManage modifyvm "$HOST_NAME" --natpf1 "shelf-server,tcp,,40081,,40081"
VBoxManage modifyvm "$HOST_NAME" --natpf1 "activemq-server,tcp,,40161,,40161"
VBoxManage modifyvm "$HOST_NAME" --natpf1 "store-ui,tcp,,40200,,40200"
VBoxManage modifyvm "$HOST_NAME" --natpf1 "postgres,tcp,,40432,,40432"
VBoxManage modifyvm "$HOST_NAME" --natpf1 "enterprise,tcp,,8080,,8080"
VBoxManage modifyvm "$HOST_NAME" --natpf1 "ent-micro-service,tcp,,8090,,8090"

date

