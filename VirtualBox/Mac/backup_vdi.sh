#!/usr/bin/env bash

#$1 is the VBox Host name.

if [ -n "$1" ]
then
  HOST_NAME="$1"
else
  echo "Enter HOST_NAME Parameter"
  exit 1;
fi

if [ -n "$2" ]
then
  BACKUP_NAME="$2.zip"
else
  BACKUP_NAME="$IMG_LOCATION/$HOST_NAME.zip"
fi

echo  "$HOST_NAME"

IMG_LOCATION="$HOME/Documents/VirtualMachines/VirtualBox/$HOST_NAME"

# HOST_NAME=Ubuntu-18.04
VDI_NAME="$IMG_LOCATION/$HOST_NAME.vdi"

echo "IMG_LOCATION: $IMG_LOCATION"
echo "HOST_NAME: $HOST_NAME"
echo "VDI_NAME: $VDI_NAME"
echo "BACKUP_NAME: $BACKUP_NAME"

date

echo "Compacting $VDI_NAME ...."
VBoxManage modifymedium --compact "$VDI_NAME"

date

echo zip "$BACKUP_NAME" "$VDI_NAME"
zip "$BACKUP_NAME" "$VDI_NAME"

date

