#!/usr/bin/env bash

#$1 is the VBox Host name.

if [ -n "$1" ]
then
  HOST_NAME="$1"
else
  echo "Enter HOST_NAME Parameter"
  exit 1;
fi


echo  "$HOST_NAME"

IMG_LOCATION="$HOME/Documents/VirtualMachines/VirtualBox/$HOST_NAME"

# HOST_NAME=Ubuntu-18.04
VMDK_NAME="$IMG_LOCATION/$HOST_NAME.vmdk"
VDI_NAME="$IMG_LOCATION/$HOST_NAME.vdi"

echo "IMG_LOCATION: $IMG_LOCATION"
echo "HOST_NAME: $HOST_NAME"
echo "VDI_NAME: $VDI_NAME"
echo "VMDK_NAME: $VMDK_NAME"


date

VBoxManage modifymedium --compact "$VDI_NAME"

date


VBoxManage clonehd "$VDI_NAME"  "$VMDK_NAME" --format vmdk  --variant Split2G

date

